package com.practice.project;

public class SimpleDotCom {
    public int[] locationCells;
    public int numOfHits = 0;

    public String checkYourself(String userGuess){
        int userGuessInt = Integer.parseInt(userGuess);
        for(int i=0; i<locationCells.length; i++){
            if(userGuessInt == locationCells[i]){
                numOfHits++;
                if(numOfHits == 3){
                    return "kill";
                }
                else
                    return "hit :-)";
            }
//            else
//                return "miss";
        }
        return "miss :-(";
    }
    public void setLocationCells(int[] locations){
        locationCells = locations;
    }
}
