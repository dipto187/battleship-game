package com.practice.project;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Game {

    public static void main(String[] args) {
        Scanner inp = new Scanner(System.in);
        SimpleDotCom dot = new SimpleDotCom();
        int numOfGuess = 0;
        int rand = (int)(Math.random()*11);
        int[] randomLoc = {rand,rand+1,rand+2};
        dot.setLocationCells(randomLoc);
        boolean end = false;
        while(end!=true){
            System.out.print("Make a guess between 1-10 --> ");
            String guess = inp.nextLine();
            String result = dot.checkYourself(guess);
            System.out.println("You "+result);
            numOfGuess++;
            if(result.equals("kill")){
                end = true;
                System.out.println("Game ends!!!");
                System.out.println("Your score is: "+numOfGuess);
            }
        }

    }
}
